<?php

$final = array();
$searchArray = array();

$url = "https://s3-us-west-2.amazonaws.com/mlo-public/credentails-test-response.json";
  
	
	//  Initiate curl
	$ch = curl_init();
	// Disable SSL verification
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	// Will return the response, if false it print the response
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// Set the url
	curl_setopt($ch, CURLOPT_URL,$url);
	// Execute
	$result=curl_exec($ch);
	// Closing
	curl_close($ch);

	$data = json_decode($result, TRUE);

	$numberOfItems = 30;

	//Begin processing data
	foreach($data as $data) {
	
		for($counter = 0; $counter <= $numberOfItems; $counter++){
	
			$final[] = $data[$counter]['labels'][0]['color'];
		
		}
  	  }
	 
	//Sanitize array and remove empty values
	$cleanedArray = array_filter($final);
	
	//count the number of times a color has been used
	$countedArray = array_count_values($cleanedArray);
	
	//sort array based on number of times a color was used
	arsort($countedArray);
		
	
	//Set up the colors and their calculated rank in its own array
	$p = 1;
	foreach($countedArray as $keys=>$values) {
		$searchArray[$keys] = $p;
		$p++;
	}
	
?>
<html>
	<body>
		<h1>Top GitHub Label Colors</h1>
		<form action="" method="post">
			<input type="text" id="color" name="color" />
			<button type="submit" name="submit" id="submit">Submit</button>
		</form>
		<div id="result">
			<?php
			
			if (isset($_POST['submit'])) {
				$color = $_POST['color'];
				
				if($color === '') {
					echo "Please input a color code";
				}
				else {
				
				$answer = $searchArray[$color].": #".$color;
				
				echo $answer;
			}
		
			}
				
			if (!isset($_POST['submit'])) {
				
				$x = 1;
			
				foreach($countedArray as $key=>$value) {
				
					echo $x.": #".$key." | Used ".$value." times<br>";
					
					$x++;
				
				} 
			}
				
			?>		
		</div>
	</body>
</html>